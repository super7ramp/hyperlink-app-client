BINDIR := /usr/bin
APPDIR := /usr/share/applications


all: app-client

app-client: app-client.c
	$(CC) -g -ggdb3 -O2 -o $@ $(CFLAGS) $<

install: app-client app-client.desktop
	install -Dm0755 app-client $(BINDIR)
	install -Dm0644 app-client.desktop $(APPDIR)

uninstall:
	rm -f $(BINDIR)/app-client
	rm -f $(APPDIR)/app-client.desktop
