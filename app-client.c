/* app-client - Program that dispatches app URIs to the particular program.  */

/* Copyright (C) 2019-2020 Free Software Foundation, Inc.  */

/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Written by Bruno Haible <bruno@clisp.org>, 2011, 2019.
 * Modified by Darshit Shah <darnir@gnu.org>, 2020
 */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/uio.h>

#define VERSION "0.0"
#define MAX_URI_LENGTH 2080 /* see Koblinger's gist */


/* Creates a client socket, by connecting to a server on the given port.  */
static int
create_client_socket (const char *hostname, int port)
{
  /* Create a client socket.  */
  int client_socket = socket (PF_INET, SOCK_STREAM, IPPROTO_TCP);
  if (client_socket < 0)
    {
      fprintf (stderr, "Failed to create socket.\n");
      exit (10);
    }
  /* Connect to the server process at the specified port.  */
  {
    struct sockaddr_in addr;

    memset (&addr, 0, sizeof (addr)); /* needed on AIX and OSF/1 */
    addr.sin_family = AF_INET;
    inet_pton (AF_INET, hostname, &addr.sin_addr);
    addr.sin_port = htons (port);

    if (connect (client_socket, (const struct sockaddr *) &addr, sizeof (addr)) < 0)
      {
        fprintf (stderr, "Failed to open socket %s:%d\n", hostname, port);
        exit (11);
      }
  }

  return client_socket;
}

static void
usage (FILE *out)
{
  printf ("Usage: printf '%%s' URI | app-client\n");
}

int
main (int argc, char *argv[])
{
  char uri[MAX_URI_LENGTH + 1];
  size_t n;
  if (argc > 1)
    {
      if (strcmp (argv[1], "--help") == 0)
        {
          usage (stdout);
          exit (0);
        }
      if (strcmp (argv[1], "--version") == 0)
        {
          printf ("app-client " VERSION "\n");
          exit (0);
        }
      n = strlen(argv[1]);
      if (n > MAX_URI_LENGTH)
        {
          fprintf (stderr, "URI too long.\n");
          exit (3);
        }
      strncpy(uri, argv[1], MAX_URI_LENGTH);
    } else {
      n = fread (uri, 1, sizeof (uri), stdin);
      if (n == 0)
        {
          fprintf (stderr, "No URI given on standard input.\n");
          exit (2);
        }
      if (n > MAX_URI_LENGTH)
        {
          fprintf (stderr, "URI too long.\n");
          exit (3);
        }
    }
  /* NUL-terminate, for simplicity.  */
  uri[n] = '\0';

  /* Verify the URI starts with "app:".  */
  if (!(n >= 4 && memcmp (uri, "app:", 4) == 0))
    {
      fprintf (stderr, "URI does not have the 'app' protocol.\n");
      exit (4);
    }

  /* Dissect the URI and verify its syntax: app://hostname:port/payload.  */
  if (!(uri[4] == '/' && uri[5] == '/'))
    {
      fprintf (stderr, "URI syntax error.\n");
      exit (5);
    }

  char *colon_pos = strchr (uri + 6, ':');
  char *slash_pos = strchr (uri + 6, '/');
  if (!(colon_pos != NULL && (slash_pos == NULL || colon_pos < slash_pos)))
    {
      fprintf (stderr, "URI syntax error.\n");
      exit (5);
    }

  const char *hostname = uri + 6;
  *colon_pos = '\0';

  if (*hostname == '\0')
    {
      fprintf (stderr, "URI syntax error.\n");
      exit (5);
    }

  char *port_str = colon_pos + 1;
  if (slash_pos != NULL)
    *slash_pos = '\0';

  long port;
  char *end_of_port;
  errno = 0;
  port = strtol (port_str, &end_of_port, 10);
  if (*end_of_port != '\0' || errno != 0 || port <= 0 || port >= 65535)
    {
      fprintf (stderr, "URI syntax error.\n");
      exit (5);
    }

  char empty_string[1] = { '\0' };
  char *payload = (slash_pos != NULL ? slash_pos + 1 : empty_string);

  /* Open the port on the hostname.  */
  char my_hostname[64];
  if (gethostname (my_hostname, sizeof (my_hostname)) >= 0
      && strcmp (my_hostname, hostname) == 0)
    /* Use localhost instead of my_hostname.  */
    hostname = "localhost";

  /* Send the payload and a newline.  */
  int client_socket = create_client_socket (hostname, port);
  size_t remaining = strlen (payload) + 1;
  payload[remaining - 1] = '\0';
  do
    {
      ssize_t written = write (client_socket, payload, remaining);
      if (written <= 0)
        {
          fprintf (stderr, "Failed to write to socket %s:%d\n", hostname, (int)port);
          exit (12);
        }
      payload += written;
      remaining -= written;
    }
  while (remaining > 0);

  exit (0);
}

